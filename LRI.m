function imResRL = LRI(I, OI, H, nite, backes, regtp, reglb, folder)
%% Lucy-Richardson Iteration with Regularization
%
% imRes = LRI(I, OI, H, nite, regtp, reglb, folder)
%
% Inputs: 
%      I     - degradated image
%      OI    - original image
%      H     - PSF
%      nite  - number of iterations (-1 for residual 0.01 stop)
%      backes- background estimation (default 0 - no background)
%      regtp - regularization type (default 0 - off)
%                  1: Total Variation   
%                  2: Tikhonov-Miller  
%                  3: RL-Conchello
%                  4: Background RL
%              reglb: regularization parameter - lambda (default 0.01)
%      folder - where to save results
%
% Output: Restored Image with RL Iteration
%
% Main references:
%    Ponti et al. "A restoration algorithm  microscopy with total variation regularization". 
%    XXI Brazilian Symposium on Computer Graphics and Image Processing, IEEE.
%    2007.
%
%    N. Dey et. al. "A deconvolution method for confocal  microscopy with total variation 
%    regularization". II Int. Symposium on Biomedical Imaging, IEEE. pp. 1223-1226, 2004.
%
%   Moacir Ponti
%   http://www.icmc.usp.br/pessoas/moacir
% -------------------------------------------------------------------------

	more off;
	% parameter verification
	if nargin < 8
	  disp('missing parameters. type help LRI for details');
	end
	
	stopr = 0
	if (nite == -1)
	    stopr = 1
	    nite = 10000 % allocate multiple values for undetermined iterations
	    disp('Lucy-Richardson (residual stopping)');
	else
	    disp(strcat('Lucy-Richardson (',strcat(num2str(nite), ' iterations)')));
	end

	% starting message
	DescExp = 'RL';

	if (regtp == 1)
	  DescExp = [DescExp '_TV_(lambda=' num2str(reglb) ')'];
	  disp(strcat('Total Variation: ON (lambda=',num2str(reglb),')'));
	elseif (regtp == 2)
	  DescExp = [DescExp '_TM_(lambda=' num2str(reglb) ')'];
	  disp(strcat('Tikhonov-Miller: ON (lambda=',num2str(reglb),')'));  
	elseif (regtp == 3)
	  DescExp = [DescExp '_ConTM_(lambda=' num2str(reglb) ')'];
	  disp(strcat('Conchello TM: ON (lambda=',num2str(reglb),')'));  
	elseif (regtp == 4)
	  DescExp = [DescExp '_BG'];
	  disp(strcat('Background version: ON'));  
	else    
	  disp('Regularization: OFF');
	end

	disp(' ');
	disp('Pre-iteration processing...');

	% flag control to perform tests 
	% if image is the original image, then it cannot perform MSE tests
	if (I == OI)
	    test = 0;
	else
	    test = 1;
	    vetISN = zeros(1,nite);
	    vetUQ = zeros(1,nite);
	    vetIDV = zeros(1,nite);
	end
	vetITE = zeros(1,nite);
	vetBP = zeros(1,nite);

	% image size
	[n,m,q] = size(I);

	% 3D laplacian mask for TM regularization
	if (regtp == 2)
	  OLP = zeros(3,3,3);
	  OLP(:,:,1) = [0  0 0;  0 -1  0; 0  0 0];
	  OLP(:,:,2) = [0 -1 0; -1  6 -1; 0 -1 0];
	  OLP(:,:,3) = [0  0 0;  0 -1  0; 0  0 0];
	end

	I = double(I);
	OI = double(OI);

	% Initialization
	o = double(abs(I));
	Bs = zeros(size(o));
	newo = o;
	residual = 1;

	% Convert psf to OTF of image size
	% OTF is Optical Transfer Function
	OTF = fftn(H);

	% clear unused variables
	clear H;

	if (test == 0)
	  clear OI;
	end

	disp('finished pre-processing.');
	disp(' ');

	% time variables (time start and current time)
	timst= clock;
	timcr= clock;
	irec = 1;

	if (backes == 1)
		% background detection and removal before restoration
		[~,Bs] = rmback(o,2,0);
	else if (backes == 2)
        	[~,Bs,bvalue]=rmback(o,2,0);
	else if (backes == 3)
        	[tmp1, tmp2, bvalue] = imhist3(I,256,0);
        	clear tmp1 tmp2;
        	[~,Bs]= rmback(o,1,bvalue);
    	end
    	end
    	end

	disp('Starting iteration...');
	disp(' ');

	tic
	% L-R iterations
	for i=1:nite
	    
	    % add background for BG version
	    if (regtp == 4) 
		%o = o + b;
	    end
	    
	    % Multiply OTF with Estimate in Frequency Domain
	    % calculating the denominator of ratio (a blurred image)
	    denom1 = OTF.*fftn(o);

	    % Ratio of Blurred Image and Estimate of Deblurred Image
	    % made in spatial domain and converted to frequency domain
	    ratio1 = fftn(I./(fftn(denom1)+Bs));

	    % Perform the product with ratio and OTF
	    % resulting in the correction vector
	    term1 = ifftn(ratio1 .* OTF);

	    clear ratio1 denom1;
	    
	    % Total Variation
	    if (regtp == 1)
		% gradient
		if (q>1)
		  [Grx,Gry,Grz] = gradient(o);
		else
		  [Grx,Gry] = gradient(newm);
		end

		% Calculate the ratio gradient/|gradient| - norm L-1   
		Gratx = Grx./sum(sum(sum(abs(Grx))));
		Graty = Gry./sum(sum(sum(abs(Gry))));

		if (q > 1) % for 3D images
		  Gratz = Grz./sum(sum(sum(abs(Grz))));
		  
		  % Calculate the divergence
		  diver = divergence(Gratx,Graty,Gratz);
		  testdiv = max(max(max(diver)));
		  diver = (diver./(testdiv));

		else
		  % Calculate the divergence
		  diver = divergence(Gratx,Graty);
		end
		
		valTV = abs(1-(reglb.* diver));
		term2 = o./ valTV;
		    
		newo = term1.*term2;

	    % Tikhonov-Miller
	    elseif (regtp == 2)
		
		lpfil = abs(ifftn(fftn(o).*fftn(OLP,[n m q])));

		valTM = (1-(reglb * lpfil));
		term2 = o ./ valTM;

		newo = term1.*term2;
		
	    else
		% Perform the product with term1 and current 'o'
		% which creates next estimate
		newo = term1.*o;
	    end
	    
	    newo = abs(newo); % remove possible imaginary values

	    % time count
	    timspite = clock-timcr;
	    timcr = clock;    
	
	    % residual calculation - stopping criteria
	    residualant = residual;
	    residual = (sum(sum(sum( abs(newo-o) )))) / sum(sum(sum(o)));
	  
	    if (test > 0)   
		if (mod(i,10) == 0) || i == nite || i == 1     
		  isnr = ISNR(OI,I,newo);
		  iuiq = uiqi3(OI, real(newo));
		  idiv = idiver(OI,newo);

		  nbpass = impb(newo);

		  vetISN(irec) = isnr;
		  vetUQ(irec) = iuiq;
		  vetIDV(irec) = idiv;
		  vetITE(irec) = i;
		  vetBP(irec) = nbpass;
		  
		  irec= irec+1;

		  if (mod(i,20) == 0) || i == nite || i == 1   
		      disp(strcat('Iteration: ', num2str(i)));
		      disp(strcat(' ISNR: ', num2str(isnr)));
		      disp(strcat(' UIQI: ', num2str(iuiq)));
		      disp(strcat(' I-Div: ', num2str(idiv)));  
		      disp(strcat(' Bandpass: ', num2str(nbpass)));             
		      disp(strcat(' -residual: ', num2str(residual)));
		      disp(strcat(' >time spent: ', num2str(timspite(4)), ':', num2str(timspite(5)), '''', num2str(timspite(6))));
		      disp(' ');
		      fflush(stdout);
		  end
		end
	    else     
		if (mod(i,10) == 0) || i == nite || i == 1
		    nbpass = impb(newo);          
		    vetBP(irec) = nbpass;
		    vetITE(irec) = i;            
		    irec= irec+1;
		    disp(strcat('Iteration: ', num2str(i)));
		    disp(strcat(' Bandpass: ', num2str(nbpass)));              
		    disp(strcat(' -residual: ', num2str(residual)));
		    disp(strcat(' >time spent: ', num2str(timspite(4)), ':', num2str(timspite(5)), '''', num2str(timspite(6))));
		    disp(' ');
		    fflush(stdout);
		end
	    end
	  
	    o = newo;

	    % save images to visualize restoration
	    if (i == nite) || (i == 1) || (mod(i,25) == 0)
	      if (i < 10)
		  itern = strcat('000',num2str(i));
	      elseif (i < 100)
		  itern = strcat('00',num2str(i));
	      elseif (i < 1000)
		  itern = strcat('0',num2str(i));
	      else
		  itern = num2str(i);
	      end
	      A(:,:,1) = double(mat2gray(o(:,round(q/2),:), [0 255]));
	      R = double(mat2gray(o(:,:,round(q/2)), [0 255]));
	      %figure; subplot(2,1,1); imshow(A,[]);
	      %      subplot(2,1,2); imshow(R,[]); 
	      imwrite(R, strcat(folder,DescExp,num2str(nite),'_',itern,'R.jpg'));
	      imwrite(A, strcat(folder,DescExp,num2str(nite),'_',itern,'A.jpg'));
	    end

	    % residual break
	    if (residual < 0.001 && (stopr == 1))
		break;
	    end
	end

	o = real(o);

	timsptot = clock-timst;

	tmptotalM = (((timsptot(4)*60)+(timsptot(5)))+(timsptot(6)))/60;

	disp(' ');
	disp(' ');
	disp(strcat(' >>>Total time spent: ', num2str(timsptot(4)), ':', num2str(timsptot(5)), '''', num2str(timsptot(6))));
	disp(' ');

	if (test == 1)
	%    figure;
	%    subplot(3,1,1);plot(1:floor(nite*0.1)+1,vetISN(1:floor(nite*0.1)+1),'b');
	%    subplot(3,1,2);plot(1:floor(nite*0.1)+1,vetBP(1:floor(nite*0.1)+1),'r');
	%    subplot(3,1,3);plot(1:floor(nite*0.1)+1,vetIDV(1:floor(nite*0.1)+1),'r');
	    data_export(folder,DescExp,vetITE,vetISN,vetUQ,vetIDV,vetBP,tmptotalM);
	end

	toc
	% Restored Image
	imResRL = o;
