function imRes = SPIS(I, H, nite, L, step, alpha, test, OI)
% -------------------------------------------------------------------------------
%  POCS-SPIS : POCS Sequence of Supports
%
%  Author: Moacir Ponti
% 
%  imRes = SPIS(I, H, nite, L, step, alpha, test, OI)
%
%  INPUT:
%    I - observed image (to be restored)
%    H - point spread function
%    nite - maximum number of iterations (0 to stopping when res < 0.001)
%    L - vector with reduction limits on the support [default = (0,0,0)]
%    step - step size for the support shrink [default = 1]
%    alpha - value to indicate change on the support
%    test - show/not show testing results during iterations [default = 1]
%    OI - original image (required when test = 1)
%   
%  OUTPUT:
%    imRes - restored image
%
%  Ponti, M. Improving Restoration of Microscopy Images using Iterative Prototypes 
%            and a Sequence of Support Constraints. ICIP 2012, p.3065-3068.
%
% --------------------------------------------------------------------------------

    more off; %% OCTAVE
    % input parameter checking
    if nargin < 3
      disp('missing parameters. type help SPIS for details');
    else
        if nargin < 7
          test = 1;  
        end
        if nargin < 6
          alpha = 0.001;  
        end
        if nargin < 5
          step = 1; 
        end
        if nargin < 4
          L = [0 0 0]; % default limit represents no reduction in support size
        end
    end

    % diplay messages
    if (nite > 0)
        disp(strcat('POCS-SPIS Lucy-Richardson (',strcat(num2str(nite), ' iterations)')));
    else
        disp(strcat('POCS-SPIS Lucy-Richardson (stops when residual < ',strcat(num2str(alpha), ')')));
    end
    disp(' ');
    disp('Pre-iteration processing...');

    % Initial values 
    [n,m,q] = size(I);   % image size
    o = I;               % first estimation of image
    Ss = ones(size(I));  % first support
    nchange = 0;         % changes on the support
    beta = alpha*2;      % beta residual value  
    R = [0 0 0];         % vector of current support reduction
    stopchange = 0;      % flag to stop support change

    % choose a big number of iterations to stop using residual criterion
    if (nite == 0)
        nite = 100000;
    end

    % Convert psf to OTF of image size
    % OTF is Optical Transfer Function
    OTF = fftn(H);

    % diplay messages
    disp(' ');
    disp('Starting iterative restoragion...');
    disp(' ');
    %fflush(stdout);
    % estimate running time (start)
    tic
    for i=1:nite
	
        % Multiply OTF with Estimate in Frequency Domain
        % calculating the denominator of ratio (a blurred image)
        denomrl = ifftn(OTF.*fftn(o));

        % Ratio of Blurred Image and Estimate of Deblurred Image
        % made in spatial domain and converted to frequency domain
        ratiorl = I ./ (denomrl);
        termrl = ifftn(OTF .* fftn(ratiorl));

        % apply RL correction in image    
        newo = real(termrl.*o);

        % residual calculation - stopping & support changing criteria
        residual = sum( abs(newo(:)-o(:)) ) / sum( o(:) );

        % condition to shrink support or stop the reductions
	if (step > 0)
	    if (residual <= beta) && (stopchange == 0)
	      [Ss,RR] = shrink_s(Ss,L,R,step);
	      if sum(sum(sum(RR~=R))) == 0
		  stopchange = 1;
	      end
	      R = RR;
	      % apply support constraint
	      newo = newo .* Ss;
	      nchange = nchange+1;
	      beta = beta - (beta*0.1); % linear decreasing
	      %beta = beta * (i/(10000/log(beta))); % exponential decreasing
	    else
	      % apply support constraint
	      newo = newo .* Ss;    
	    end
	end

        % set the new estimation to be used on the next estimate
        o = real(newo);
        
        % stopping criteria (for undetermined number of iterations)
        if (nite == 100000) && ((residual <= alpha)) % || (alpha > beta))
	   % perform a final shrink using half the limit size and step+1
	   L = single(uint8(L.*1.5));
	   [Ss,RR] = shrink_s(Ss,L,R,step+1);
	   o = o .* Ss;
           break;
        end

        if (mod(i,30) == 0) || i == 1   
            disp(strcat('Iteration: ', num2str(i)));
            if (test == 1)   
               isnr = ISNR(OI,I,newo);
               iuiq = uiqi3(OI, real(newo));
               idiv = idiver(OI,newo);
               nbpass = impb(newo);

               if (mod(i,30) == 0) || i == nite || i == 1   
                   disp(strcat(' ISNR: ', num2str(isnr)));
                   disp(strcat(' UIQI: ', num2str(iuiq)));
                   disp(strcat(' I-Div: ', num2str(idiv)));  
                   disp(strcat(' Bandpass: ', num2str(nbpass)));             
               end
            end
            disp(strcat(' -residual: ', num2str(residual)));
            disp(strcat(' -current beta: ', num2str(beta)));
            disp(strcat(' -changes on the support: ', num2str(nchange)));
	    if (stopchange == 1) 
		disp(strcat(' --reached limit '));
	    end
            disp(' ');
            %fflush(stdout);%OCTAVE
        end
    end
    toc
    % estimate running time (end)

    disp(' ');
    disp(strcat('Total number of iterations: ', num2str(i)));
    % compute evaluation and show
    if (nargin >= 8)
        isnr = ISNR(OI,I,newo);
        iuiq = uiqi3(OI, real(newo));
        idiv = idiver(OI,newo);
        nbpass = impb(newo);
        disp(strcat(' ISNR: ', num2str(isnr)));
        disp(strcat(' UIQI: ', num2str(iuiq)));
        disp(strcat(' I-Div: ', num2str(idiv)));  
        disp(strcat(' Bandpass: ', num2str(nbpass)));             
    end
    disp(strcat(' -final residual: ', num2str(residual)));
    disp(strcat(' -final beta: ', num2str(beta)));
    disp(strcat(' -changes on the support: ', num2str(nchange)));
    disp(' ');
    
    % return restored Image
    imRes = real(o);
return
