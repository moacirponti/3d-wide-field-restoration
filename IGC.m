function imRes = IGC(I, H, nite, lambda, alpha, pj, test, OI, folder)
% -------------------------------------------------------------------------------------
%  Iterative Gradient Deconvolution with Constraints (IGC)
%
%  Reference:
%  "Image Restoration using Gradient Iteration and Constraints for Band Extrapolation"
%  submitted to the IEEE Journal of Selected Topics in Signal Processing
%  Authors: Moacir Ponti and Elias S Helou.
%
%  imRes = IGC(I, H, nite, lambda, alpha, test, OI)
%
%  INPUT:
%    I - observed image (to be restored)
%    H - point spread function
%    nite - maximum number of iterations (0 to stopping when res < alpha)
%    lambda - lambda value for the subgradient projection (default = 0.03)
%    alpha - stopping criterion (for residual)
%    pj - vector with projection settings [p1,p2,p3,p4] with 0 for no projection
%	    default = [1,0,0,1]:
%		p2 - method for background removal
%			1 - subtraction before restoration
%			2 - first peak support
%			3 - sequence of subtractions
%		p3 - Gerchberg-Papoulis projection (0/1)
%		p4 - Non-negativity projection (0/1)
%    OI - original image (required when test = 1)
%    folder - folder to store results (required when test = 1)
%
%  OUTPUT:
%    imRes - restored image
% ------------------------------------------------------------------------------------

    more off; %% OCTAVE --- remove if using matlab

    % input parameter checking
    if nargin < 3
      disp('missing parameters. type help SPIS for details');
    else
        if nargin < 7
          test = 1;
        end
	if nargin < 6
	  pj = [0, 0, 1];
	end
        if nargin < 5
          alpha = 0.001;
        end
	if nargin < 4
          lambda = 0.01;
	end
    end

    % diplay messages
    if (nite > 0)
        disp(strcat('POCS-SPID Lucy-Richardson (',strcat(num2str(nite), ' iterations)')));
    else
        disp(strcat('POCS-SPID Lucy-Richardson (stops when residual < ',strcat(num2str(alpha), ')')));
    end

    DescExp = 'SPI';
    if (pj(2) == 1)
      DescExp = [DescExp '_1-SBR'];
      disp('Background detection 1 (Subtraction before restoration)');
    elseif (pj(2) == 2)
      DescExp = [DescExp '_2-FPS'];
      disp('Background detection 2 (Firsk peak support)');
    elseif (pj(2) == 3)
      DescExp = [DescExp '_3-SOS'];
      disp('Background detection 3 (Sequence of subtractions)');
    else    
      DescExp = [DescExp '_0-RL'];
      disp('Background detection: OFF');
    end

    if (pj(3) == 1)
      DescExp = [DescExp '_GP'];
      disp('Gerchberg-Papoulis projection');
    end
 
    if (pj(4) == 1)
      DescExp = [DescExp '_NN'];
      disp('Non-negativity constraint');
    end
    fflush(stdout);

 
    disp(' ');
    disp('Pre-iteration processing...');
    disp(' ');

   % Initial values
    [n,m,q] = size(I);   % image size
    o = double(I);               % first estimation of image
    I = o;
    Bs = ones(size(I));  % first background support
    nchange = 0;         % changes on the support
    beta = alpha*2;      % beta residual value
    stopchange = 0;      % flag to stop support change
    bvalue = 0;
    irec = 1;

    % choose a big number of iterations if
    % chooses to stop using residual criterion
    if (nite == 0)
        nite = 100000;
    end

    % Convert psf to OTF of image size
    % OTF is Optical Transfer Function
    OTF = fftn(double(H));

    % Compute target log-likelihood:
    Ax = ifftn( OTF .* fftn( o ) );
    Lu = sum( sum( sum( real( Ax ) - I .* log( real( Ax ) ) ) ) );
    Ll = sum( sum( sum( I - I .* log( I+0.01 ) ) ) );
    Lt = lambda *  Lu + ( 1 - lambda ) * Ll;

    if (pj(2) == 1)
	% background detection and removal before restoration
        o=rmback(o,2,0);
    else if (pj(2) == 2)
        [~,Bs,bvalue]=rmback(o,2,0);
    else if (pj(2) == 3)
        [tmp1, tmp2, bvalue] = imhist3(I,256,0);
        clear tmp1 tmp2;
        [o,Bs]= rmback(o,1,bvalue);
	o = double(o);
        cur_bmse = bmse(I,H,o,bvalue);
    end
    end
    end

    % diplay messages
    disp(' ');
    disp('Starting iterative restoration...');
    disp(' ');
    fflush(stdout);  %% OCTAVE --- remove if using matlab

    % estimate running time (start)
    tic

    for i=1:nite

        % Multiply OTF with Estimate in Frequency Domain
        % calculating Ax (a blurred image)
        Ax = ifftn( OTF .* fftn( o ) );

        % Gradient of log-likelihood function, wich is given
        % by $A^T( 1 - b ./ Ax )$:
        grad = real( ifftn( OTF .* fftn( 1 - I ./ Ax ) ) );

	% projection (1)
        nsqgrad = sumsq( grad( :) );
        if ( nsqgrad ~= 0 )
          L = sum( sum( sum( real( Ax ) - I .* log( real( Ax ) ) ) ) );
          s = 0.5 * max( [ 0 ( L - Lt ) ] ) ./ nsqgrad;
          % Apply gradient correction in image
          newo = abs(o - s * grad);
        end

	% projection(2)
	if (pj(2) == 2)
		% first peak support
		Bsl = double(Bs);
		Bsl(Bs==0) = lambda;
		newo2 = Bsl.*newo;
	else if (pj(2) == 3 && stopchange == 0) 
		% sequence of subtractions
		Bs = (newo-1)>0;
	        bvalue = bvalue + 1;
	        prev_bmse = cur_bmse
	        cur_bmse = bmse(I,H,newo,bvalue)

        	% error increases
	        if (cur_bmse >= prev_bmse)
	        	Bs = (o>0);      % previous background
			bvalue = bvalue-1;
	                stopchange = 1;  % stop further subtractions
	                backarea = sum(Bs(:))./(n*m*q);
	                disp(strcat(' stopped>: ',num2str(bvalue), ' bg area: ', num2str(backarea)));
        	        disp(' ');
		else 
			Bsl = double(Bs);
			Bsl(Bs==0) = lambda;
			newo2 = Bsl.*newo;
		endif
	endif
	endif
	%figure; imshow(newo2(:,:,32),[]); pause;

	% projection(3)
	if (pj(3) == 1 && stopchange == 0) 
		newo = gerpap_proj(newo, newo2, Bs);
	else if (pj(2) > 1 && stopchange == 0)
		newo = newo2;
	endif
	endif

	% projection (4) non-negativity
	if (pj(4) == 1) 
		newo(newo<0) = 0;
	endif

	% residual calculation - stopping & support changing criteria
	residual = sum( abs(newo(:)-o(:)) ) / sum( o(:) );

	if (test > 0)   
		if (mod(i,10) == 0) || i == nite || i == 1     
			isnr = ISNR(OI,I,newo);
			iuiq = uiqi3(OI, real(newo));
			idiv = idiver(OI,newo);
			nbpass = impb(newo);

			vetISN(irec) = isnr;
			vetUQ(irec) = iuiq;
			vetIDV(irec) = idiv;
			vetITE(irec) = i;
			vetBP(irec) = nbpass;
			irec= irec+1;
			disp(strcat('Iteration: ', num2str(i)));
		        disp(strcat(' ISNR: ', num2str(isnr)));
		        disp(strcat(' UIQI: ', num2str(iuiq)));
		        disp(strcat(' I-Div: ', num2str(idiv)));  
		        disp(strcat(' Bandpass: ', num2str(nbpass)));             
		        disp(strcat(' -residual: ', num2str(residual)));
		        disp(' ');
		        fflush(stdout);

	  	      	A(:,:,1) = double(mat2gray(o(:,round(q/2),:), [0 255]));
	      		R = double(mat2gray(o(:,:,round(q/2)), [0 255]));
	      		imwrite(R, strcat(folder,DescExp,num2str(nite),'_',num2str(i),'R.jpg'));
	      		imwrite(A, strcat(folder,DescExp,num2str(nite),'_',num2str(i),'A.jpg'));
		end
	else     
		if (mod(i,20) == 0) || i == nite || i == 1
		    nbpass = impb(newo);          
		    iuiq = uiqi3(I, real(newo));
		    vetBP(irec) = nbpass;
		    vetITE(irec) = i;            
	 	    vetUQ(irec) = iuiq;
		    irec= irec+1;
		    disp(strcat('Iteration: ', num2str(i)));
		    disp(strcat(' Bandpass: ', num2str(nbpass)));              
		    disp(strcat(' -residual: ', num2str(residual)));
		    disp(' ');
		    fflush(stdout);
	      	    A(:,:,1) = double(mat2gray(o(round(n*0.4375),:,:), [0 255]));
	      	    R = double(mat2gray(o(:,:,round(q/2)), [0 255]));
	      	    imwrite(R, strcat(folder,DescExp,num2str(nite),'_',num2str(i),'R.jpg'));
	      	    imwrite(A, strcat(folder,DescExp,num2str(nite),'_',num2str(i),'A.jpg'));
		end
	end

	% set the new estimation to be used on the next estimate
	o = newo;

	% stopping criteria
	% based on alpha value (or after a huge number of iterations)
	if ((residual <= alpha) && (nite == 100000)) || i == nite
	    o = o .* Bs;
	    break;
	end

    end

    % estimate running time (end)
    timesp = toc;

    disp(' ');
    disp(strcat('Total number of iterations: ', num2str(i)));
    disp(strcat(' >>>Total time spent: ', num2str(timesp)));
    disp(strcat(' -final residual: ', num2str(residual)));
    disp(' ');

    if (test == 1)
	    data_export(folder,DescExp,vetITE,vetISN,vetUQ,vetIDV,vetBP,timesp);
    else
 	    data_exportR(folder,DescExp,vetITE,vetUQ,vetBP,timesp);
   end


    % return restored Image
    imRes = single(o);
return
