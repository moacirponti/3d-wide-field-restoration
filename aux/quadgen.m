% -------------------------------------------------------------
% Quad Generation
%
% sintax: Q = quadgen(sizS,disS)
%
% variables:
%   Q    : quad generated
%   sizS : [x y z] - image size
%   disS : quad size 
% -------------------------------------------------------------

function Q = quadgen(sizS,disS)

% standard input
if (nargin < 1)
   disp('arguments required');
end

if (sizS == [disS disS disS])

    Q = ones(sizS);
   
elseif (sizS > [disS disS disS])

    Q = zeros(sizS);

    x = sizS(1);
    y = sizS(2);
    z = sizS(3);

    lat = round((x - disS)/2);

    Q(lat+1:x-lat,lat+1:x-lat,lat+1:x-lat) = ones(disS,disS,disS);

elseif (sizS(1:2) > [disS disS])

    Q = zeros(sizS);

    x = sizS(1);
    y = sizS(2);
    z = sizS(3);

    latx = round((x - disS)/2);
    laty = round((y - disS)/2);
    latz = round(round((1-(disS/sizS(1)))*sizS(3))/2);

    disZ = sizS(3) - (latz*2);

    Q(latx+1:x-latx,laty+1:y-laty,latz+1:z-latz) = ones(disS,disS,disZ);

else
    Q = ones(sizS);
    disp('quad must be lower than the image size');
end