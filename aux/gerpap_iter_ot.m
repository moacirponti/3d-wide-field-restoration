function EI = gerpap_iter_ot(I,CF1,CF2,MF,SP)
% -------------------------------------------------------------
% Gerchberg-Papoulis Algorithm for Image Extrapolation
%
% sintax: gerpap_iter_ot(I,Fs,Ss,filter)
%
% variables:
%   I     : image
%   Fs : frequency support
%   Ss : spatial support
%
% -------------------------------------------------------------

if nargin < 5
   SP = 0;
end
if nargin < 4
   MF = 0;
end

[t1b,t2b,t3c] = size(I);

% creates a circle /sphere mask with radius defined by user
% CF1 will be used on frequency cut-off limit

% I2 = imextension(single(I),8,1);
% CF1 = imextension(single(CF1),8,1);
% CF2 = imextension(single(CF2),8,1);

% storing known spectra
F = fftn(double(I));

% storing original DC level
fDC = F(1:2);
F = fftshift(F);

   % Spatial constraint (image support)
   I(CF2 == 0) = 0;
   
   clear CF2;

   % Fourier transform - Frequency Domain 
   Y = fftshift(fftn(double(I)));
   % Real part of frequencies beyond the cuf-off limit
   Yr = Y.*(1-CF1);

   % median filter
   if (MF == 1)
      for ft=1:t3c
          Tp = real(ifftn(fftshift(Yr(:,:,ft))));
          Yr(:,:,ft) = fftn(medfilt2(Tp));
      end
   % frequency domain median filter
   elseif (MF == 2)
      for ft=1:t3c
          Yr(:,:,ft) = medfilt2(real(Yr(:,:,ft))) + i*medfilt2(imag(Yr(:,:,ft)));
      end
   % mean filter
   elseif (MF == 3)
      f=ones(3,3)/9;
      for ft=1:t3c
          %Tp = real(ifftn(fftshift(Yr(:,:,ft))));
          Tp = ifftn(fftshift(Yr(:,:,ft)));
          Yr(:,:,ft) = fftn(filter2(f,Tp));
      end
   % frequency domain mean filter
   elseif (MF == 4)
      f=ones(3,3)/9;
      for ft=1:t3c
          Yr(:,:,ft) = filter2(f,real(Yr(:,:,ft))) + i*filter2(f,imag(Yr(:,:,ft)));
      end
   % cut-off frequencies
   elseif (MF == 5)
      for ft=1:t3c
          Yr(:,:,ft) = 0;
      end
   end
   
   % Assembling the known spectra with filtered extrapolated spectra
   %Ys = (Yr+((i*imag(Y)).*abs(CF1-1))) + F.*(CF1);
   
   Ys = Yr.*(1-CF1) + F.*(CF1);

   if (SP == 1)
      tMean = mean(mean(mean(real(Ys))));
      idLim = find(real(Ys) < (tMean/2));
      Ys(idLim) = 0;
   end   
   
   % Restoring the original DC level 
   Ys = fftshift(Ys);
   Ys(1:2) = fDC;

   % Inverse Fourier Transform - Spatial Domain
   %I2 = real(ifftn(Ys));
   
   %EI = imcorta(I2,8,1);
   EI = real(ifftn(Ys));