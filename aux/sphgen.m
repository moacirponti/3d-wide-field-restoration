% -------------------------------------------------------------
% Sphere Generation
%
% sintax: S = sphgen(size)
%
% variables:
%   S    : sphere generated
%   sizS : [x y z] - image size
%   r    : sphere radius
% -------------------------------------------------------------

function S = sphgen(sizS, r)

% standard input
if nargin < 1
   disp('image required');
end

[x,y,z] = meshgrid(-r:r,-r:r,-r:r);
se = ((x/r).^2 + (y/r).^2 + (z/r).^2) <= 1;

S = zeros(sizS);

x = sizS(1);
y = sizS(2);
z = sizS(3);

centro = [round(x/2) round(y/2) round(z/2)];

for i=1:x
    for j=1:y
        for k=1:z
            local = [i j k];
            disA = sum((centro-local).^2).^0.5;
            if disA < disS
               S(i,j,k) = 1;
            end
        end
    end
end
