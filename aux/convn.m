## Copyright (C) 2008 Søren Hauberg
## 
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

## -*- texinfo -*-
## @deftypefn {Function File} @var{C} = convn(@var{A}, @var{B}, @var{shape})
## @math{N}-dimensional convolution of matrices @var{A} and @var{B}.
##
## The size of the output is determined by the @var{shape} argument. This can be
## any of the following strings
##
## @table @asis
## @item "full"
## The full convolution result is returned. The size out of the output is
## @code{size(@var{A})+size(@var{B})-1}. This is the default behaviour.
## @item "same"
## The central part of the convolution result is returned. The size out of the
## output is the same as @var{A}.
## @item "valid"
## The valid part of the convolution is returned. The size of the result is
## @code{max(size(@var{A})-size(@var{B})+1, 0)}.
## @end table
##
## @seealso{conv, conv2}
## @end deftypefn

function C = convn(A, B, shape = "full")
  ## Check input
  if (nargin < 2)
    error("convn: not enough input arguments");
  endif
  if (!ismatrix(A) || !ismatrix(B) || ndims(A) != ndims(B))
    error("convn: first and second arguments must be matrices of the same dimensionality");
  endif
  if (!ischar(shape))
    error("convn: third input argument must be a string");
  endif
  if (!any(strcmpi(shape, {"full", "same", "valid"})))
    error("convn: invalid shape argument: '%s'", shape);
  endif
  
  ## Should we swap A and B?
  ## XXX: Should we also swap in any of the non-full cases?
  if (numel(B) > numel(A) && strcmpi(shape, "full"))
    tmp = A;
    A = B;
    B = tmp;
  endif
  
  ## Pad A
  switch (lower(shape))
    case "full"
      A = pad(A, size(B)-1, size(B)-1);
    case "same"
      A = pad(A, floor((size(B)-1)/2), ceil((size(B)-1)/2));
  endswitch
  
  ## Perform convolution
  C = __convn__(A, B);
endfunction

## Helper function that performs the padding
function A = pad(A, left, right)
  c = class(A);
  for dim = 1:ndims(A)
    l = r = size(A);
    l(dim) = left(dim);
    r(dim) = right(dim);
    A = cat(dim, zeros(l, c), A, zeros(r, c));
  endfor
endfunction
