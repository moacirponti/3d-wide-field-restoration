function [Ir,Bs,bv] = rmback(I,tp,bgval)
%% Remove Image Background
%  
%  Input: 
%     I: image
%     tp: detection type
%     bgval: parameter according to option
%
%  Output
%     Ir: image with removed background
%     Bs: image background support
%     bv: background value found
%
%  Author:
%     Moacir Ponti
%     http://www.icmc.usp.br/~moacir
%     ponti@usp.br
%
%3D sphere generation:
%[x,y,z] = meshgrid(-r:r,-r:r,-r:r);
%se = ((x/r).^2 + (y/r).^2 + (z/r).^2) <= 1;

   if nargin < 2
      'missing parameters. type "help imremoveback" for details';
   end
   
   % simple subtraction
   if (tp == 1)
      Ir = I-bgval;
      Ir(Ir<0)=0;
      Bs = (Ir~=0);
      bv = bgval;
   end
   % first peak subtraction
   if (tp == 2)
      %A = medfilt3(I,7);
      [bv,ht] = imhist3(I, 128, 0);
      if (bv>1) 
	bv = bv-1;
      end
      Ir = I-bv;
      Ir(Ir<0) = 0;
      Bs = (Ir~=0);
   end
   % first peak + opening
   if (tp == 3)
      [bv,ht] = imhist3(round(I), round(max(I(:))/2) );
      Ir= I-(bv);
      Ir(Ir<0)=0;
      Bs = (Ir~=0);
      [n,m,q] = size(Bs);
      r= bgval;

      [x,y] = meshgrid(-r:r,-r:r);
      se = ((x/r).^2 + (y/r).^2) <= 1;
      for k=1:q
	  Bs(:,:,k) = imopen(Bs(:,:,k), se);
	  Bs(:,:,k) = imclose(Bs(:,:,k), se);
      end
      Ir = Bs.*I;
   end
   % first peak + opening + gaussian filtering
   if (tp == 4)
      [bv,ht] = imhist3(round(I), round(max(I(:))/2) );
      Ir= I-(bv);
      Ir(Ir<0)=0;
      Bs = (Ir~=0);
      [n,m,q] = size(Bs);
      r= bgval;

      [x,y] = meshgrid(-r:r,-r:r);
      se = ((x/r).^2 + (y/r).^2) <= 1;
      for k=1:q
	  Bs(:,:,k) = imopen(Bs(:,:,k),se);
	  Bs(:,:,k) = imclose(Bs(:,:,k),se);
      end

      gsize = round(bgval*2);
      if (mod(gsize,2) == 0)
         gsize = gsize-1;
      end
      Bs = smooth3(single(Bs),'gaussian',gsize,1.5);
      Ir = Bs.*Ir;
      figure; plot(1:round(max(I(:))/2),ht);
%        pause
   end
   % non-zero simple subtraction
   if (tp == 5)
      Ir2=round(I);
      Ir2(Ir2==0) = 100000;
      bv = round(min(Ir2(:)));
      Ir = round(I)-bv;
      Ir(Ir<0)=0;
      Bs = Ir~=0;
   end
   % first half peak subtraction
   if (tp == 6)
      bv = imhist3(round(I), round(max(I(:))/2));
      bv = round((bv-1)/2);
      Ir= I-(bv);
      Ir(Ir<0)=0;
      Bs = (Ir~=0);
   end
   % otsu lower threshold background removal
   if (tp == 7)
      nbins = round(max(I(:)));
      [bv,ht] = imhist3(round(I), nbins);
      figure; plot(1:nbins, ht); pause;
      [n,m,q] = size(I);
      w1 = 1/2;
      w2 = 1/2;
      m1 = 0;
      m2 = 0;
      npix = n*m*q;
      sT = zeros(1,nbins);
      for i=1:nbins
          w1 = sum(ht(1:i))/npix;
          m1 = sum((0:(i-1)).*ht(1:i)) / sum(ht(1:i));
          s1 = sum(((0:i-1)-m1).^2.*ht(1:i)) / sum(ht(1:i));

          w2 = sum(ht(i+1:nbins))/npix;
          m2 = sum((i:255).*ht(i+1:nbins)) / sum(ht(i+1:nbins));
          s2 = sum(((i:255)-m2).^2.*ht(i+1:nbins)) / sum(ht(i+1:nbins));
          sT(i) = (w1*s1)+(w2*s2);
      end
      [val,bv] = min(sT);
      
      Ir= I-(bv-1);
      Ir(Ir<0)=0;
      Bs = (Ir~=0);
   end

   disp(strcat(' -Background value detected: ',num2str(bv)));
return
