% -------------------------------------------------------------------
% Poisson image noise
%
% ------------------------------------------------------------------

function value = impoiss(lambda)

      L = exp(-double(lambda));
      p = 1.0;
      k = 0;
      while (p > L)
		k = k+1;
		p = p * unifrnd(0.0001, 1, 1, 1);
      end
	  
      value = k-1;
      if (value < 0) 
         value = 0;
      end
      if (value > 255) 
         value = 255;
      end
return