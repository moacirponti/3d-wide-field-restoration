function [peakvalue,hist3D,firstvalue] = imhist3(I,bins,show)
%% 3D Image Histogram
%  
%  Input: 
%     I: image
%     show: 1 - show histogram, 0 - do not show (default)
%
%  Output
%     hist3D: array with intensities frequencies
%     firstmax: maximum value of the first 50 intensities in the image
%
%  Author:
%     Moacir P. Ponti Jr.
%     Signal and Image Processing Group / Computing Department - UFSCar
%     +55-3351-8579 
%     http://www.dc.ufscar.br/~gapis
%     moacirponti@gmail.com
%

   if nargin < 3
      show = 0;
   end
%     I = double(I./max(I(:)));
%     I = uint8(I.*bins);
   
   av = uint8(mean(I(:))); % mean value
   
   hist3D = zeros(1,bins);
   
   % populate array with frequency of each intensity
   for i=1:bins
       hist3D(i) = sum(sum(sum(I == (i-1))));
   end
   
   % find the first maximum in the image
   [maxtotal,peakvalue] = max(hist3D(1:av));

   nonzerohist = hist3D(hist3D>0);
   firstvalue = find(hist3D==nonzerohist(1));
   firstvalue = firstvalue(1)-1;
   
%     peakvalue = floor(peakvalue*(256/bins))-1

   % show bar graph with intensities
   if (show == 1)
      figure; bar(1:bins,hist3D);
   end
return