function E = gerpap_iter(f,Bs)
% -------------------------------------------------------------
% Gerchberg-Papoulis Algorithm for Image Extrapolation
%
% sintax: gerpap_iter(OI, CI)
%
% variables:
%   I : original image
%   Bs : spatial support
%
% -------------------------------------------------------------

[n,m,q] = size(f);

% storing known spectra
F = fftn(double(f));

% storing original DC level
DClevel = F(1);

% Spatial constraint (image support)
fs = f.*Bs;
   
% Fourier transform of constrained image 
E = fftn(double(fs));

% Find image practical bandpass
[NF] = size(F(:));
vP = max(abs(F(2:NF)));
Fs = F./vP;
BPf = (abs(Fs) >= 0.01);
%disp(strcat('BS region size: ', num2str(sum(BPf(:)))));

% Assembling the known spectra with extrapolated spectra

E = E.*(1-BPf) + F.*(BPf);

% Restoring the original DC level 
E(1) = DClevel;

E = real(ifftn(E));