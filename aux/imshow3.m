function imshow3(f, s)
% -------------------------------------------------------------
% Fast 3d image visualization
%
% sintax imshow3(f, val)
%
% variables:
%   f : original image
%   s : stack to show
%
% -------------------------------------------------------------

figure; imshow(double(f(:,:,s)), [ 0 255]);