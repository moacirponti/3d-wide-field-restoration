function [M,RR] = shrink_s(S,L,R,step)
% Image Shrink
%
% [M,RR] = shrink_s(S,L,R,step)
%   S - image
%   L - limits for shinkage
%   R - vector of regions 
%   
%   M - output image
%   RR - amount of reduction

[n,m,q] = size(S);
M = S;
RR = R;
if ( R(1)+step < L(1) )
   M( R(1)+1:R(1)+step          , :, :) = 0;
   M( n-R(1):-1:n-(R(1)+step-1) , :, :) = 0;
   RR(1) = R(1)+step;
end   
if ( R(2)+step < L(2) )
   M( :, R(2)+1:R(2)+step          , :) = 0;
   M( :, m-R(2):-1:m-(R(2)+step-1) , :) = 0;
   RR(2) = R(2)+step;
end   
if ( R(3)+step < L(3) )
   M( :, :, R(3)+1:R(3)+step         ) = 0;
   M( :, :, q-R(3):-1:q-(R(3)+step-1)) = 0;
   RR(3) = R(3)+step;
end

a = max(RR);
if (mod(a,2)==0)
   a =a+1;
end
if (R(3)~=RR(3))
  % M = smooth3(M,'gaussian',a,1.5);
else
   if (R(1)~=RR(1) || R(2)~=RR(2))
      for i=1:q
          HH = fspecial('gaussian',a,1.5);
          M(:,:,i) = imfilter(M(:,:,i),HH,'symmetric');
      end
   end
end