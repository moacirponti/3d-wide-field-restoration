function data_export(PathName,DescExp,ITER,ISNR,UIQI,IDIV,BPASS, tempoM)

time = clock;
Str = [PathName '/Res_' DescExp '_' num2str(time(3)) '-' num2str(time(2)) '_' num2str(time(4)) '-' num2str(time(5)) num2str(ITER(end)) '.txt']; 

if exist(Str,'file') 
   delete(Str);
end

if exist(Str,'file') 
   errordlg('"Document must be closed."', 'Error.') 
   return 
end

fid = fopen(Str, 'wt');

fprintf(fid, '%s \n',DescExp); 

fprintf(fid, 'Iterations; ISNR; UIQI; IDIV; BandPass\n'); 
  
ind = 5;
t = size(ISNR,2);

%Saving values
for i=1:t
   if (ISNR(i) == 0) && (UIQI(i) == 0)  && (IDIV(i) == 0) && (BPASS(i) == 0)
      break;
   end
   fprintf(fid, '%d; %0.4f; %0.4f; %0.4f; %d \n', ITER(i), ISNR(i), UIQI(i), IDIV(i), BPASS(i));
   ind=ind + 1;
end

fprintf(fid, 'Time: %.3f\n', tempoM);

fclose(fid);

return