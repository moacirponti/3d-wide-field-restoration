function g = imdegrade(f, H, bgvalue)
% -------------------------------------------------------------
% Degrade image
%
% sintax: imdegrade(f, H, bgvalue)
%
% variables:
%   f : original image
%   H : PSF
%   bgvalue: background value
% -------------------------------------------------------------


fb = f;
fb(f == 0) = bgvalue;

FF = fftn(fb);
HF = fftn(H);
fBlur = real(ifftn(HF.*FF));

g = poissrnd(fBlur);