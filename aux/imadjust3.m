function G = imadjust3(I)

	[N,M,O] = size(I);
	low_in = min(I(:));
	high_in = max(I(:));
	G = zeros(N,M,O);
	for i=1:O
		G(:,:,i) = imadjust(I(:,:,i),[low_in; high_in]);
	end

	G = double(G.*255);
