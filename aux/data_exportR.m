function data_exportR(PathName,DescExp,ITER,UIQI,BPASS, tempoM)
% Data export for real data

time = clock;
Str = [PathName '/Res_' DescExp '_' num2str(time(3)) '-' num2str(time(2)) '_' num2str(time(4)) '-' num2str(time(5)) num2str(ITER(end)) '.txt']; 

if exist(Str,'file') 
   delete(Str);
end

if exist(Str,'file') 
   errordlg('"Document must be closed."', 'Error.') 
   return 
end

fid = fopen(Str, 'wt');

fprintf(fid, '%s \n',DescExp); 

fprintf(fid, 'Iterations; UIQI; BandPass\n'); 
  
ind = 5;
t = size(UIQI,2);

%Saving values
for i=1:t
   if (UIQI(i) == 0 && BPASS(i) == 0)
      break;
   end
   fprintf(fid, '%d; %0.4f; %d \n', ITER(i), UIQI(i), BPASS(i));
   ind=ind + 1;
end

fprintf(fid, 'Time: %.3f\n', tempoM);

fclose(fid);

return
