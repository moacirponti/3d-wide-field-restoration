% -------------------------------------------------------------------
% Background Mean Square Error (BMSE) entre imagens
%    background estimation error
%
% Sintax: error = bmse(I,psf,IP,bvalue)
%
% ------------------------------------------------------------------

function error = bmse(I,psf,IP,bvalue)
  
    if nargin < 4
       disp('duas imagens requeridas');
       return
    end

    [n,m,k] = size(I);
    [x,y,z] = size(IP);

    if (n ~= x) || (y ~= m)|| (k ~= z)
       disp('imagens devem possuir tamanhos iguais');
       return
    end

    I = double(round(I));
    IP = double(round(IP));
    psf = double(psf);    

    FF = fftn(IP);
    HF = fftn(psf);
    f = real(ifftn(HF.*FF));
    
    Bs = bvalue.*(IP==0);

    error = sum(sum(sum( (I-(f+Bs)).^2 )));
    
    %error = sum(sum(sum( abs(I-(f+bvalue)) )))/(n*m*k);
    %error = sum(sum(sum( (I-(f+bvalue)).^2 )))/(n*m*k);

return
