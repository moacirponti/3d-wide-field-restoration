%FUNCAO QUE CALCULA OS COEFICIENTES DE CORRELACAO NORMALIZADOS DE UMA IMAGEM
%
%USAGE  x = correlation(f,orientacao)
%
%f = imagem (grayscale)
%orientacao: 1 = correla��o nas linhas
%            2 = correla��o nas colunas

function x = correlation(f,orientacao)

[m n p] = size(f);
x = zeros(1,10);
f = double(f);

if (orientacao == 1) %correla�ao nas linhas"

   %computa correla�ao em x
   for k = 0:m-1

       sum = 0;
       for i = 1:2:m-1
           for j = 1:2:n-k
               sum = sum+(f(i,j)*f(i,j+k));
           end
       end

       sum = sum/(m*(n-k));
       x(k+1) = sum;
   end
elseif (orientacao == 2)
   %computa correla�ao em y
   for k = 0:n

       sum = 0;
       for i = 1:2:m-k
           for j = 1:2:n
               sum = sum+(f(i,j,:)*f(i+k,j,:));
           end
       end

       sum = sum/(m*(n-k));
       x(k+1) = sum;
   end
elseif (orientacao == 3)
   %computa correla�ao em z
   for k = 0:p

       sum = 0;
       for i = 1:m-k
           for j = 1:n
               sum = sum+(f(:,j,i)*f(:,j,i+k));
           end
       end

       sum = sum/(m*(n-k));
       x(k+1) = sum;
   end   
end

x = x/x(1);
