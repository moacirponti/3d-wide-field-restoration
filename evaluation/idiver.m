% -------------------------------------------------------------------
% I-Divergence 
%
%    Reference:
%       I. Csiszar, "Why least squares and maximun entropy?". 
%       The Annals of Statistics, 19, pp.2032-2066, 1991.
%
%    also cited by: 
%       N. Dey et. al. "A deconvolution method for
%       confocal microscopy with total variation regularization".
%       II International Symposium on Biomedical Imaging, IEEE.
%       pp. 1223-1226, 2004.
%
% Sintaxe: idm = idiver(I,IP)
%      I  - original image
%      IP - processed image
% ------------------------------------------------------------------

function idm = idiver(I,IP)
  
if nargin < 2
   'duas imagens requeridas'
   return
end

[n,m,k] = size(I);
[x,y,z] = size(IP);

if (n ~= x) || (y ~= m)|| (k ~= z)
   'imagens devem possuir tamanhos iguais'
end

I = abs(I);
IP = abs(IP);

I(I<1) = 1;
IP(IP<1) = 1;

ratc = I./IP;
logc = log(ratc);

term1 = I .* log(ratc);

pre1 = (term1 - (I - IP));

%idm = sum(sum((sum(pre1))));
%idm = round(mean(mean(sum(pre1))));
idm = mean(mean(mean(pre1)));

% idm = abs(1-idm);

return