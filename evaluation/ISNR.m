% isnr = ISNR(imgReal,imgReal_degraded,imgReal_restored)

function isnr = ISNR(imgReal,imgReal_degraded,imgReal_restored)
% older version: 
%   aux1 = double(imgReal_degraded) - double(imgReal);
%   aux2 = double(imgReal_restored) - double(imgReal);
%   isnr = 10*log10(norm(aux1(:))^2/norm(aux2(:))^2);

aux1 = (single(imgReal_degraded) - single(imgReal)).^2;
aux2 = (single(imgReal_restored) - single(imgReal)).^2;

% aux1 = aux1.^2;
% aux2 = aux2.^2;
% aux1 = sum(sum(aux1));
% aux2 = sum(sum(aux2));
aux1 = sum(sum(sum(aux1)));
aux2 = sum(sum(sum(aux2)));

isnr = 10*log10(aux1/aux2);