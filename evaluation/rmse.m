% -------------------------------------------------------------------
% Raiz Erro Medio Quadrático (RMSE) entre imagens
%    mede a diferenca entre a imagem real e uma imagem processada
%
% Sintaxe: erro = rmse(I-IP)
%
%      I    - imagem original
%      IP   -  imagem processada
% ------------------------------------------------------------------

function erro = rmsei(I,IP)
  
if nargin < 2
   'duas imagens requeridas'
   return
end

[n,m,k] = size(I);
[x,y,z] = size(IP);

if (n ~= x) || (y ~= m)|| (k ~= z)
   'imagens devem possuir tamanhos iguais'
end

I = im2single(I);
IP = im2single(IP);

if z == 0
   erro = sqrt(mean(mean(abs(I-IP).^2)));
else
   erro = sqrt(mean(mean(mean(abs(I-IP).^2))));
end

return
