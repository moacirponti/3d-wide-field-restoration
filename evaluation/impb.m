function [nel,idx,energy,pbZ] = impb(I)
% image practical passband

F = fftn(I);
energy = F(1);
[a,b,c] = size(F);
[NF] = size(F(:));
vP = max(abs(F(2:NF)));
Fs = fftshift(F)./vP;
idx = abs(Fs) >= 0.01;
nel = uint32(sum(idx(:)));

%  if nargout > 2
%     pbZ(:,:,1) = abs(Fs(a/2,:,:));
%     pbZ(pbZ<=0.002) = 0;
%     pbZ=pbZ';
%     pbZ=medfilt2(pbZ,[2 2]);
%     idx=medfilt3b(idx,[3 3 3]);
%  end

nel = single(nel);