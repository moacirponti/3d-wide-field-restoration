% image extrapolated frequencies

function [nef,rnul] = imef(I,PSF)

F = fftn(I);
OTF = fftn(PSF);
out_supp = abs(OTF) <= 0.0000001;
ext_supp = F.*out_supp;

nef  = length(find(ext_supp > 0));
rnul = length(find(out_supp > 0));