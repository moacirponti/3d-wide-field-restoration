function  quality = uiqi3(f,r,block_uiqi)

% Universal Image Quality Index for Three-Dimensional Images
%
%   quality = uiqi3(img1, img2);
%
%   Input : an original image and a test image of the same size
%   Output: (1) an overall quality index of the test image, with a value
%               range of [-1, 1].
%
%  Moacir P Ponti Jr. / 2010
%  Universidade de Sao Paulo

[a, b, c] = size(f);

% block of partial uiqi test
% -- must be higher for sparse images
if (a == c)
   if nargin < 3
      block_uiqi = round(c/3);
   end
   st = 1;
elseif (c < round(a/2));
   if nargin < 3
      block_uiqi = round(c*.75);
   end
   st = 2;
end

% defining step of sampling through the 3D image
sdi = 8;
while (mod(st,2) ~= 0) 
   st = c/sdi;
   sdi = sdi-1;
end

% sampling through the 3D image
s_qual = 0;
for i=1:st:c
    a_qual = img_qi(f(:,:,i),r(:,:,i),block_uiqi);
    s_qual = [s_qual a_qual];
end

quality = mean(s_qual(s_qual>0));
